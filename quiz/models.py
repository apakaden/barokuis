from django.db import models
from django.contrib.postgres.fields import ArrayField


class Question(models.Model):
    text = models.TextField()
    STYLE_CHOICES = [
        ('RADIO', 'Radio'),
        ('TEXT', 'Text'),
        ('SELECT', 'Select'),
        ('RANGE', 'Range Slider'),
        ('DATEYM', 'Date YM'),
        ('DATEYMD', 'Date YMD'),
    ]
    style = models.CharField(max_length=16, choices=STYLE_CHOICES, default='TEXT')
    placeholder = models.CharField(max_length=128, null=True, blank=True, default=None)
    helper_text = models.TextField(null=True, blank=True, default=None)
    parent_question = models.ForeignKey("self", on_delete=models.CASCADE, null=True, blank=True, default=None)
    parent_answer = models.CharField(max_length=128, null=True, blank=True, default=None)
    options = ArrayField(models.CharField(max_length=128), default=list, blank=True)
    range_min= models.IntegerField(null=True, blank=True, default=None)
    range_max= models.IntegerField(null=True, blank=True, default=None)
    range_step= models.CharField(max_length=8, null=True, blank=True, default=None)

    def __str__(self):
        return self.text[:min(50, len(self.text))]

    class Meta:
        ordering = ['id']
    

class AnswerSheet(models.Model):
    duration = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.pk)


class Answer(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    answer_sheet = models.ForeignKey(AnswerSheet, on_delete=models.CASCADE, related_name='answers')
    text = models.CharField(max_length=1024)

    def __str__(self):
        return str(self.answer_sheet.pk) + ' ' + self.text[:min(50, len(self.text))]

class Info(models.Model):
    title = models.CharField(max_length=128)
    subtitle = models.CharField(max_length=256, null=True, blank=True, default=None)
    description = models.TextField(null=True, blank=True, default=None)
    exit_message_head = models.CharField(max_length=128, null=True, blank=True, default=None)
    exit_message_content = models.TextField(null=True, blank=True, default=None)
    
    def __str__(self):
        return self.title