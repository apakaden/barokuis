import json
import datetime
from django.shortcuts import render
from django.http import JsonResponse
from .models import Question, Answer, AnswerSheet, Info
from django.views.decorators.csrf import csrf_exempt


def index(request):
    questions = Question.objects.all()
    infos = Info.objects.all()
    context = {
        'questions': questions,
        'infos': infos
    }
    html = 'quiz/index.html'
    return render(request, html, context)


def finish(request):
    html = 'quiz/finish.html'
    infos = Info.objects.all()
    context = {
        'infos': infos
    }
    return render(request, html, context)


@csrf_exempt
def saveAnswer(request):
    request_body = json.loads(request.body)
    answers = request_body['answer']
    duration = request_body['duration']
    sheet = AnswerSheet.objects.create(duration=duration)

    for question_id, text in answers.items():
        question = Question.objects.get(id=question_id)
        if question.parent_question is None:
            answer = Answer.objects.create(question_id=question_id, answer_sheet=sheet, text=text)
        else:
            if question.parent_answer == answers[str(question.parent_question.id)]:
                answer = Answer.objects.create(question_id=question_id, answer_sheet=sheet, text=text)

    return JsonResponse({'status': 'success'})