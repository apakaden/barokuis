from django.contrib import admin
from .models import Question, Answer, AnswerSheet, Info


class QuestionAdmin(admin.ModelAdmin):
    model = Question


class AnswerAdmin(admin.TabularInline):
    model = Answer


class AnswerSheetAdmin(admin.ModelAdmin):
    model = AnswerSheet
    inlines = [AnswerAdmin]

class InfoAdmin(admin.ModelAdmin):
    model = Info

admin.site.register(Question, QuestionAdmin)
admin.site.register(AnswerSheet, AnswerSheetAdmin)
admin.site.register(Info, InfoAdmin)