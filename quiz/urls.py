from django.urls import path
from . import views


urlpatterns = [
    path('', views.index),
    path('save/', views.saveAnswer),
    path('finish/', views.finish),
]