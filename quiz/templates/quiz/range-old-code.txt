<div class="d-flex justify-content-between align-item-center">
	<a v-html="r_min"></a><input v-model="answer" class="custom-range mx-1" type="RANGE" v-bind:min="r_min" v-bind:max="r_max" v-bind:step="r_step"  @change="changeAnswer"><a v-html="r_max"></a>
</div>

w ruben tores radio to slider
<!-- Radio to Slider -->
		<link rel="stylesheet" type="text/css" href="{% static 'radio-to-slider/css/radios-to-slider.css' %}">
		<script type="text/javascript" src="{% static 'radio-to-slider/js/jquery.radios-to-slider.js' %}"></script>
		
		
		
Inline Radio with label below item-center

				<div v-if="type =='RADIOIL'" class="d-flex justify-content-between align-item-center">
					<div class="custom-radio d-inline-block" v-for="option in options" v-bind:value="option">
						<input class="custom-control-input m-auto d-block" type="radio" v-model="answer" v-bind:value="option" v-bind:name="id" v-bind:id="option + '_' + id" @change="changeAnswer">
						<label class="custom-control-label rz-radio-inline m-auto d-block" v-bind:for="option + '_' + id"><% option %></label>
					</div>
				</div>

.rz-radio-inline::before{
	position: relative !important;
	top: auto !important;
	left: auto !important;
}

.rz-radio-inline::after{
	top: 0rem !important;
	left: auto !important;
}