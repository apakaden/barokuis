# Generated by Django 3.0.1 on 2020-01-22 08:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0007_auto_20191228_0215'),
    ]

    operations = [
        migrations.CreateModel(
            name='Info',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=128)),
                ('subtitle', models.CharField(blank=True, default=None, max_length=256, null=True)),
                ('description', models.TextField(blank=True, default=None, null=True)),
                ('exit_message_head', models.CharField(blank=True, default=None, max_length=128, null=True)),
                ('exit_message_content', models.TextField(blank=True, default=None, null=True)),
            ],
        ),
        migrations.AddField(
            model_name='question',
            name='placeholder',
            field=models.CharField(blank=True, default=None, max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='answer',
            name='text',
            field=models.CharField(max_length=1024),
        ),
    ]
